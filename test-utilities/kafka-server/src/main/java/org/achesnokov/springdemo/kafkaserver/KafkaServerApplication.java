package org.achesnokov.springdemo.kafkaserver;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.output.OutputFrame;
import org.testcontainers.containers.output.ToStringConsumer;

import java.net.UnknownHostException;


/**
 * @author achesnokov
 */
@SpringBootApplication
//@EnableScheduling
public class KafkaServerApplication implements ApplicationRunner {


    Network network = Network.newNetwork();


    GenericContainer zookeeper = new FixedHostPortGenericContainer("bitnami/zookeeper:latest")
            .withFixedExposedPort(2181, 2181)
            .withNetwork(network)
            .withNetworkAliases("zookeeper")
            .withEnv("ALLOW_ANONYMOUS_LOGIN", "yes");


    GenericContainer kafka = new FixedHostPortGenericContainer("bitnami/kafka:latest")
            .withFixedExposedPort(9092, 9092)
            .withNetwork(network)
            .withNetworkAliases("kafka")
            .withEnv("KAFKA_ZOOKEEPER_CONNECT", "zookeeper:2181")
            .withEnv("ALLOW_PLAINTEXT_LISTENER", "yes")
            .withEnv("KAFKA_LISTENERS", "PLAINTEXT://:9092")
            .withEnv("KAFKA_ADVERTISED_LISTENERS", "PLAINTEXT://localhost:9092")
            .withLogConsumer(new ToStringConsumer() {
                @Override
                public void accept(OutputFrame outputFrame) {
                    System.out.print("\uD83D\uDEA6 " + outputFrame.getUtf8String());
                }
            });


    public KafkaServerApplication() throws UnknownHostException {
    }


    public static void main(String[] args) {
        SpringApplication.run(KafkaServerApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        zookeeper.start();
        kafka.start();
    }

}
