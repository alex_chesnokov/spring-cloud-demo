package org.achesnokov.springdemo.mongodbserver;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.output.OutputFrame;
import org.testcontainers.containers.output.ToStringConsumer;

import java.util.concurrent.CountDownLatch;


/**
 * @author achesnokov
 */
@SpringBootApplication
//@EnableScheduling
public class MongoDbServerApplication implements ApplicationRunner {


    Network network = Network.newNetwork();

    GenericContainer mongoDb = new FixedHostPortGenericContainer("bitnami/mongodb:latest")
            .withFixedExposedPort(27017, 27017)
            .withEnv("MONGODB_USERNAME", "demo")
            .withEnv("MONGODB_PASSWORD", "demo")
            .withEnv("MONGODB_DATABASE", "opinionated-quotes")
            .withLogConsumer(new ToStringConsumer() {
                @Override
                public void accept(OutputFrame outputFrame) {
                    System.out.print("\uD83D\uDEA6 " + outputFrame.getUtf8String());
                }
            });
    ;



    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        SpringApplication.run(MongoDbServerApplication.class, args);
        latch.await();
    }
/*

    @Scheduled(initialDelay = 10000, fixedRate = 30000)
    public void test() {
        System.out.println(mongoDb);
    }
*/

    @Override
    public void run(ApplicationArguments args) throws Exception {
        mongoDb.start();
        System.out.println(mongoDb);
    }

}
