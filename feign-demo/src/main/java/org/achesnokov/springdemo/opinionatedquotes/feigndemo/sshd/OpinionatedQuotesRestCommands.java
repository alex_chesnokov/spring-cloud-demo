package org.achesnokov.springdemo.opinionatedquotes.feigndemo.sshd;

import org.achesnokov.springdemo.opinionatedquotes.feigndemo.clients.OpinionatedQuotesRestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Collectors;

import sshd.shell.springboot.autoconfiguration.SshdShellCommand;

@Component
@SshdShellCommand(value = "quotes", description = "execute remote call to OpinionatedQuotesRest service")
public class OpinionatedQuotesRestCommands {

    @Autowired
    private OpinionatedQuotesRestClient opinionatedQuotesRestClient;


    @SshdShellCommand(value = "top", description = "return top n quotes")
    public String top(String num){
        Integer n = num==null? null : Integer.valueOf(num);

        return opinionatedQuotesRestClient
                .topList(n)
                .stream()
                .map(Object::toString)
                .collect(Collectors.joining("\n"));

    }

    @SshdShellCommand(value = "id", description = "return quote by id")
    public String id(String id){
        if(id==null){
            return "id value is required!";
        }

        return Optional.ofNullable(opinionatedQuotesRestClient.getById(id)).map(Object::toString).orElse("Not found");
    }
}
