package org.achesnokov.springdemo.opinionatedquotes.feigndemo;

import org.achesnokov.springdemo.opinionatedquotes.feigndemo.clients.OpinionatedQuotesRestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class FeignDemoApplication{

    @Autowired
    private OpinionatedQuotesRestClient opinionatedQuotesRestClient;

    public static void main(String args[]){
        new SpringApplicationBuilder(FeignDemoApplication.class).run(args);
    }

}

