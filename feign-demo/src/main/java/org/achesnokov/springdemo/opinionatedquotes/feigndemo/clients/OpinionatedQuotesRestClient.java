package org.achesnokov.springdemo.opinionatedquotes.feigndemo.clients;

import org.achesnokov.springdemo.opinionatedquotes.domain.OpinionatedQuote;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "opinionated-quotes-rest", fallback = OpinionatedQuotesRestClientFallback.class)
public interface OpinionatedQuotesRestClient {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    List<OpinionatedQuote> topList();

    @RequestMapping(method = RequestMethod.GET, value = "/")
    List<OpinionatedQuote> topList(@RequestParam(name="n", required = false) Integer count);

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    OpinionatedQuote getById(@PathVariable("id") String id);

}
