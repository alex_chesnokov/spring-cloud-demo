package org.achesnokov.springdemo.opinionatedquotes.feigndemo.clients;

import org.achesnokov.springdemo.opinionatedquotes.domain.OpinionatedQuote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

public class OpinionatedQuotesRestClientFallback implements OpinionatedQuotesRestClient{
    private static final Logger LOGGER = LoggerFactory.getLogger(OpinionatedQuotesRestClientFallback.class);

    @Override
    public List<OpinionatedQuote> topList() {
        LOGGER.error("Cannot load Opinionated Quotes");
        return Collections.emptyList();
    }

    @Override
    public List<OpinionatedQuote> topList(Integer count) {
        return this.topList();
    }

    @Override
    public OpinionatedQuote getById(String id) {
        LOGGER.error("error of loading OpinionatedQuote by id: "+id);;
        return null;
    }
}
