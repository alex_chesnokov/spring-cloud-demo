package org.achesnokov.springdemo.opinionatedquotes.loader;

import org.achesnokov.springdemo.opinionatedquotes.loader.cfg.OpinionatedQuotesLoaderService;
import org.achesnokov.springdemo.opinionatedquotes.domain.Quote;
import org.achesnokov.springdemo.opinionatedquotes.utils.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestToUriTemplate;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;


@RunWith(SpringRunner.class)
@RestClientTest
@ContextConfiguration(classes = {OpinionatedQuotesLoaderService.class})
@TestPropertySource(properties = { "opinionated-quotes.quotes-per-request = 10"})
public class OpinionatedQuotesServiceTest {

    @Value("${opinionated-quotes.request-uri}")
    private String requestUri;

    @Value("${opinionated-quotes.quotes-per-request}")
    private int quotesPerRequest;

    @Autowired
    private MockRestServiceServer mockServer;

    @Autowired
    private OpinionatedQuotesLoaderService opinionatedQuotesService;

    private QuotesTestData quotesTestData = new QuotesTestData();

    @Test
    public void getQuotesTest() {
        mockServer.expect(ExpectedCount.once(), requestToUriTemplate(requestUri, quotesPerRequest))
                .andExpect(method(HttpMethod.GET))
                .andRespond(
                        withStatus(HttpStatus.OK)
                                .contentType(MediaType.APPLICATION_JSON)
                                .body(quotesTestData.randomString(quotesPerRequest)));

        List<Quote> quotes = opinionatedQuotesService.getQuotes();
        assertEquals(quotesPerRequest, quotes.size());
    }
}
