package org.achesnokov.springdemo.opinionatedquotes.loader.cfg;

import org.achesnokov.springdemo.opinionatedquotes.domain.Quote;
import org.achesnokov.springdemo.opinionatedquotes.domain.QuoteCollection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RefreshScope
public class OpinionatedQuotesLoaderService {

    @Value("${opinionated-quotes.request-uri}")
    private String requestUri;

    @Value("${opinionated-quotes.quotes-per-request}")
    private Integer quotesPerRequest;

    private RestTemplate restTemplate;

    public OpinionatedQuotesLoaderService(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }

    public List<Quote> getQuotes(){
        return Optional
                .ofNullable(restTemplate.getForObject(requestUri, QuoteCollection.class, quotesPerRequest))
                .map(QuoteCollection::getQuotes)
                .orElseGet(Collections::emptyList);
    }
}
