package org.achesnokov.springdemo.opinionatedquotes.loader;

import org.achesnokov.springdemo.opinionatedquotes.domain.Quote;
import org.achesnokov.springdemo.opinionatedquotes.loader.cfg.OpinionatedQuotesLoaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;


@SpringBootApplication
@EnableScheduling
public class OpinionatedQuotesLoaderApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(OpinionatedQuotesLoaderApp.class);

    @Value("${spring.kafka.topics.opinionated-quotes}")
    private String topicOpinionatedQuotes;

    @Autowired
    private KafkaTemplate<String, Quote> quotePublisher;

    @Autowired
    private OpinionatedQuotesLoaderService opinionatedQuotesService;

    public static void main(String args[]){
        SpringApplication.run(OpinionatedQuotesLoaderApp.class, args);
    }

    @Scheduled(initialDelayString = "${opinionated-quotes.scheduler.initial-delay}", fixedRateString = "${opinionated-quotes.scheduler.fixed-rate}")
    public void loadQuotes(){
        List<Quote>  quotes = opinionatedQuotesService.getQuotes();
        quotes.forEach(q -> {
            quotePublisher.send(topicOpinionatedQuotes, q);
        });
        quotePublisher.flush();
        quotes.forEach(q -> LOGGER.debug("{}", q));
    }
}
