package org.achesnokov.springdemo.opinionatedquotes.rest.controllers;


import org.achesnokov.springdemo.opinionatedquotes.domain.OpinionatedQuote;
import org.achesnokov.springdemo.opinionatedquotes.rest.repository.OpinionatedQuotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.data.domain.Sort.Direction.DESC;

@RestController
public class OpinionatedQuotesRestController {

    @Autowired
    private OpinionatedQuotesRepository opinionatedQuotesRepository;

    @GetMapping("/{id}")
    public Mono<OpinionatedQuote> getById(@PathVariable String id){
        return opinionatedQuotesRepository.findById(id);
    }

    @GetMapping("/")
    public Flux<OpinionatedQuote> getTopList(@RequestParam(name = "tags", required = false) Collection<String> tags,
                                             @RequestParam(name="n", required = false) Integer count){
        int recordsNum = count == null || count > 10? 10 : count;

        Pageable pageable = PageRequest.of(0, recordsNum, Sort.by(DESC, "timestamp"));

        return CollectionUtils.isEmpty(tags)?
                opinionatedQuotesRepository.getPage(pageable) :
                opinionatedQuotesRepository.getByTags(tags, pageable);
    }


}
