package org.achesnokov.springdemo.opinionatedquotes.rest.repository;

import org.achesnokov.springdemo.opinionatedquotes.domain.OpinionatedQuote;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import java.util.Collection;

import reactor.core.publisher.Flux;

public interface OpinionatedQuotesRepository extends ReactiveMongoRepository<OpinionatedQuote, String> {
    @Query("{ id: { $exists: true }}")
    Flux<OpinionatedQuote> getPage(Pageable pageable);

    @Query("{ tags: { $all: ?0} }")
    Flux<OpinionatedQuote> getByTags(Collection<String> tags, Pageable pageable);

}
