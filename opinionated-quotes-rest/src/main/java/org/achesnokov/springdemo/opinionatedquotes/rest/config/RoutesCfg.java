package org.achesnokov.springdemo.opinionatedquotes.rest.config;

import org.achesnokov.springdemo.opinionatedquotes.domain.Quote;
import org.achesnokov.springdemo.opinionatedquotes.domain.OpinionatedQuote;
import org.achesnokov.springdemo.opinionatedquotes.rest.repository.OpinionatedQuotesRepository;
import org.achesnokov.springdemo.opinionatedquotes.utils.QuotesTestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Configuration
public class RoutesCfg {
    private static QuotesTestData quotesTestData = new QuotesTestData();

    @Autowired
    private OpinionatedQuotesRepository quotesRepository;

    @Bean
    public RouterFunction<ServerResponse> routes() {
        return
                nest(accept(APPLICATION_JSON_UTF8),
                        route()
//                                .GET("/", req -> ok().body(quotesRepository.findAll(), OpinionatedQuote.class))
                                .PUT("/", req -> req.bodyToMono(OpinionatedQuote.class)
                                        .map(quotesRepository::save)
                                        .flatMap(qq -> ok().body(qq, OpinionatedQuote.class)))
                                .POST("/", req -> req.bodyToMono(Quote.class)
                                        .map(OpinionatedQuote::createFrom)
                                        .map(quotesRepository::insert)
                                        .flatMap(qq -> ok().body(qq, OpinionatedQuote.class)))
                                .build());
    }

}
