package org.achesnokov.springdemo.opinionatedquotes.rest;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@EnableReactiveMongoRepositories
@EnableMongoAuditing
@EnableDiscoveryClient
public class OpinionatedQuotesRestApplication {

    public static void main(String args[]){
        new SpringApplicationBuilder(OpinionatedQuotesRestApplication.class)
                .web(WebApplicationType.REACTIVE)
                .run(args);
    }

/*
    public static void main(String[] args) {
        HttpServer
                .create()
                .host("localhost")
                .port(8080)
                .handle(new ReactorHttpHandlerAdapter(toHttpHandler(getRouter())))
                .bindUntilJavaShutdown(Duration.ofSeconds(30),null);
    }
*/

}

