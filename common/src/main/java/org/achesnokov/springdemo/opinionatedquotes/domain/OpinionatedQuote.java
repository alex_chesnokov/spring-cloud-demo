package org.achesnokov.springdemo.opinionatedquotes.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("quotes")
public class OpinionatedQuote extends Quote {
    @Id
    private String id;
    private long timestamp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }


    public String toString(){
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public static OpinionatedQuote createFrom(Quote q){
        OpinionatedQuote quote = new OpinionatedQuote();

        quote.timestamp = System.currentTimeMillis();
        quote.setAuthor(q.getAuthor());
        quote.setLang(q.getLang());
        quote.setQuoteText(q.getQuoteText());
        quote.setTags(q.getTags());

        return quote;
    }
}
