package org.achesnokov.springdemo.opinionatedquotes.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class QuoteCollection {

    private List<Quote> quotes;

    @JsonCreator
    public QuoteCollection(@JsonProperty("quotes") List<Quote> quotes){
        this.quotes = quotes;
    }

    public List<Quote> getQuotes() {
        return quotes;
    }

}
