package org.achesnokov.springdemo.opinionatedquotes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Quote {
    private List<String> tags = new ArrayList<>();
    private String quoteText;
    private String lang;
    private String author;


    public List<String> getTags() {
        return tags;
    }

    @JsonProperty("quote")
    public String getQuoteText() {
        return quoteText;
    }

    public String getLang() {
        return lang;
    }

    public String getAuthor() {
        return author;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void setQuoteText(String quoteText) {
        this.quoteText = quoteText;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String toString(){
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
