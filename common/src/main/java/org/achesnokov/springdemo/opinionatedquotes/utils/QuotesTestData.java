package org.achesnokov.springdemo.opinionatedquotes.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.achesnokov.springdemo.opinionatedquotes.domain.Quote;
import org.achesnokov.springdemo.opinionatedquotes.domain.QuoteCollection;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class QuotesTestData {
    private static String quotesDataResource = "/quotes-test-data.json";
    private Random random = new Random();
    private List<Quote> data = new ArrayList<>();

    public QuotesTestData(){
        try {
            data.addAll(load(quotesDataResource));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private List<Quote> load(String resource) throws IOException, URISyntaxException {
        return new ObjectMapper().readValue(this.getClass().getResource(resource), QuoteCollection.class).getQuotes();
    }

    public Quote random(){
        return data.get(random.nextInt(data.size()));
    }

    public List<Quote> random(int size){
        int limit = size < data.size()? size : data.size();
        return IntStream
                .generate(() -> random.nextInt(data.size()))
                .distinct()
                .limit(limit)
                .mapToObj(data::get)
                .collect(Collectors.toList());
    }

    public String randomString(){
        return asString(() -> Collections.singletonList(random()));
    }

    public String randomString(int size){
        return asString(() -> random(size));
    }

    private String asString(Supplier<List<Quote>> getQuotes){
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(new QuoteCollection(getQuotes.get()));
        } catch (JsonProcessingException e) {
            return "{}";
        }
    }
}
