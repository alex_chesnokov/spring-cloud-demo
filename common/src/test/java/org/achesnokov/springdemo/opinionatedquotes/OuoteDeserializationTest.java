package org.achesnokov.springdemo.opinionatedquotes;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.achesnokov.springdemo.opinionatedquotes.domain.Quote;
import org.achesnokov.springdemo.opinionatedquotes.domain.QuoteCollection;
import org.achesnokov.springdemo.opinionatedquotes.utils.QuoteBuilder;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class OuoteDeserializationTest {

    @Test
    public void deserializeTest() throws IOException {
        QuoteCollection quotes = new ObjectMapper().readValue(json, QuoteCollection.class);
        System.out.println(quotes);
    }

    private List<Quote> quotes(){
        return Arrays.asList(
                QuoteBuilder.aQuote().tags("one", "two", "three").lang("en").author("author1").quoteText("Some Text").build(),
                QuoteBuilder.aQuote().tags("one", "two", "three", "four").lang("en").author("author2").quoteText("Some Text again").build(),
                QuoteBuilder.aQuote().tags("three", "four").lang("en").author("author3").quoteText("Some Text again and again").build()
        );
    }

    private String json = "\n" +
            "          {\n" +
            "            \"quotes\": [{\n" +
            "              \"tags\": [\"ethics\", \"evolution\", \"transhumanism\", \"future\", \"paradise engineering\", \"humans\"],\n" +
            "              \"quote\": \"Too many of our preferences reflect nasty behaviours and states of mind that were genetically adaptive in the ancestral environment. Instead, wouldn't it be better if we rewrote our own corrupt code?\",\n" +
            "              \"lang\": \"en\",\n" +
            "              \"author\": \"David Pearce\"\n" +
            "            }, {\n" +
            "              \"tags\": [\"science\", \"transhumanism\", \"future\", \"altruism\", \"paradise engineering\"],\n" +
            "              \"quote\": \"[T]rue hedonic engineering, as distinct from mindless hedonism or reckless personal experimentation, can be profoundly good for our character. Character-building technologies can benefit utilitarians and non-utilitarians alike. Potentially, we can use a convergence of biotech, nanorobotics and information technology to gain control over our emotions and become better (post-)human beings, to cultivate the virtues, strength of character, decency, to become kinder, friendlier, more compassionate: to become the type of (post)human beings that we might aspire to be, but aren't, and biologically couldn't be, with the neural machinery of unenriched minds. Given our Darwinian biology, too many forms of admirable behaviour simply aren't rewarding enough for us to practise them consistently: our second-order desires to live better lives as better people are often feeble echoes of our baser passions.\",\n" +
            "              \"lang\": \"en\",\n" +
            "              \"author\": \"David Pearce\"\n" +
            "            }]\n" +
            "          }"
            ;

}
