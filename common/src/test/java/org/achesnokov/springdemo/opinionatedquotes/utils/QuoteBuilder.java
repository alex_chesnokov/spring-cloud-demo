package org.achesnokov.springdemo.opinionatedquotes.utils;

import org.achesnokov.springdemo.opinionatedquotes.domain.Quote;

import java.util.ArrayList;
import java.util.Arrays;

public final class QuoteBuilder {
    ArrayList<String> tags = new ArrayList<>();
    private String quoteText;
    private String lang;
    private String author;

    private QuoteBuilder() {
    }

    public static QuoteBuilder aQuote() {
        return new QuoteBuilder();
    }

    public QuoteBuilder tags(String... tags) {
        this.tags.addAll(Arrays.asList(tags));
        return this;
    }

    public QuoteBuilder quoteText(String quoteText) {
        this.quoteText = quoteText;
        return this;
    }

    public QuoteBuilder lang(String lang) {
        this.lang = lang;
        return this;
    }

    public QuoteBuilder author(String author) {
        this.author = author;
        return this;
    }

    public Quote build() {
        Quote quote = new Quote();
        quote.setTags(tags);
        quote.setQuoteText(quoteText);
        quote.setLang(lang);
        quote.setAuthor(author);
        return quote;
    }
}
