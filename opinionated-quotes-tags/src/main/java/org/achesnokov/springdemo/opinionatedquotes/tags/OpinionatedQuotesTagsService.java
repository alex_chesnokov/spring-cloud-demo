package org.achesnokov.springdemo.opinionatedquotes.tags;

import org.achesnokov.springdemo.opinionatedquotes.tags.cfg.KafkaBindingCfg;
import org.apache.kafka.streams.kstream.Windowed;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyWindowStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class OpinionatedQuotesTagsService {
    @Autowired
    private InteractiveQueryService interactiveQueryService;


    public Map<String, Long> tagsStatistics(){
        ReadOnlyWindowStore<String, Long> store = interactiveQueryService.getQueryableStore(KafkaBindingCfg.TAG_STORE, QueryableStoreTypes.<String, Long>windowStore());
        long timeTo = System.currentTimeMillis();
        long timeFrom = timeTo - TimeUnit.MINUTES.toMillis(1);

        Map<String, Long> result = new HashMap<>();
        try(KeyValueIterator<Windowed<String>, Long> it = store.fetchAll(timeFrom, timeTo)){
            it.forEachRemaining(rec -> {
                result.compute(rec.key.key(),(k,v) -> v == null?rec.value:v+rec.value);
            });
        }

        return result;
    }
}
