package org.achesnokov.springdemo.opinionatedquotes.tags;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class OpinionatedQuotesTagsApplication {

    public static void main(String args[]){
        new SpringApplicationBuilder(OpinionatedQuotesTagsApplication.class)
                .web(WebApplicationType.REACTIVE)
                .run(args);
    }
}

