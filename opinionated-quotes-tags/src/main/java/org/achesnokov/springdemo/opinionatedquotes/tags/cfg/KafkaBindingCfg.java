package org.achesnokov.springdemo.opinionatedquotes.tags.cfg;

import org.achesnokov.springdemo.opinionatedquotes.domain.Quote;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Serialized;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.binder.kafka.streams.InteractiveQueryService;

@EnableBinding(KafkaBindingCfg.OpinionatedQuotesProcessor.class)
public class KafkaBindingCfg {
    public static String TAG_STORE = "tags_statistics";

    @Autowired
    private InteractiveQueryService queryableStoreRegistry;

    @StreamListener("input")
    public void process(KStream<String, Quote> input){
        input
                .flatMapValues(Quote::getTags)
                .map((k, v) -> new KeyValue<>(v, v))
                .groupBy((k, v) -> v, Serialized.with(Serdes.String(), Serdes.String()))
                .windowedBy(TimeWindows.of(60_000).advanceBy(1000))
                .count(Materialized.as(TAG_STORE));
    }

    static interface OpinionatedQuotesProcessor {
        @Input("input")
        KStream<?, ?> input();
    }

}

