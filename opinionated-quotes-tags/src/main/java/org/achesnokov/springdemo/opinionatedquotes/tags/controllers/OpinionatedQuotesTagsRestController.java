package org.achesnokov.springdemo.opinionatedquotes.tags.controllers;

import org.achesnokov.springdemo.opinionatedquotes.tags.OpinionatedQuotesTagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import reactor.core.publisher.Mono;

@RestController
public class OpinionatedQuotesTagsRestController {

    @Autowired
    private OpinionatedQuotesTagsService opinionatedQuotesTagsService;

    @GetMapping("/tags")
    public Mono<Map<String, Long>> tagStatistics(){
        return Mono.just(opinionatedQuotesTagsService.tagsStatistics());
    }
}
