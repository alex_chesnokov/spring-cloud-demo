package org.achesnokov.springdemo.opinionatedquotes.saver;


import org.achesnokov.springdemo.opinionatedquotes.domain.Quote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@SpringBootApplication
@EnableBinding(Sink.class)
@EnableDiscoveryClient
public class OpinionatedQuotesSaverApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(OpinionatedQuotesSaverApp.class);

    @Autowired
    private OpinionatedQuotesSaverService opinionatedQuotesSaverService;

    @StreamListener(Sink.INPUT)
    public void handle(Quote quote) {
        opinionatedQuotesSaverService
                .save(quote)
                .subscribe(q -> LOGGER.info(q.toString()));
    }

    public static void main(String args[]){
        SpringApplication.run(OpinionatedQuotesSaverApp.class, args);
    }
}
