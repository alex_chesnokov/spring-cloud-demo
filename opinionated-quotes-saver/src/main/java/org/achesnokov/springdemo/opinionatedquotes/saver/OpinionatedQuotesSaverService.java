package org.achesnokov.springdemo.opinionatedquotes.saver;


import org.achesnokov.springdemo.opinionatedquotes.domain.OpinionatedQuote;
import org.achesnokov.springdemo.opinionatedquotes.domain.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.URI;

import reactor.core.publisher.Mono;

@Service
public class OpinionatedQuotesSaverService {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private WebClient webClient;


    @Bean
    public WebClient webClient(){
        return WebClient.builder().build();
    }

    public URI opinionatedQuotesRestUri(){
        return discoveryClient
                .getInstances("opinionated-quotes-rest")
                .stream().findFirst()
                .map(ServiceInstance::getUri)
                .orElse(null);
    }


    public Mono<OpinionatedQuote> save(Quote quote){
        return webClient
                .post()
                .uri(opinionatedQuotesRestUri())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .body(Mono.just(quote), Quote.class)
                .retrieve()
                .bodyToMono(OpinionatedQuote.class);
    }

}
